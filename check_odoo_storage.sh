#!/bin/bash

# Copyright © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

OUTPUT=""
PERF_DATA=""
RETURN_CODE="0"
for file in `ls /mnt/vdb/backup/odoo/*.latest`
do
    CONTAINER=$(grep "Container:" $file | cut -d ":" -f 2  | tr -d " ")
    BYTES=$(grep "Bytes:" $file | cut -d ":" -f 2  | tr -d " ")
    OBJECTS=$(grep "Objects:" $file | cut -d ":" -f 2  | tr -d " ")
    LAST_FILE=$(tail -2 $file | grep gpg)
    if [ `date -d "${LAST_FILE:5:19}" +%s` -gt `date -d '1 day ago' +%s` ]
    then
        OUTPUT+="OK: Container: $CONTAINER, Size: $BYTES, # Files: $OBJECTS, Last-Modified: ${LAST_FILE:5:19}\n"
    else 
        OUTPUT+="NOK: Container: $CONTAINER, Size: $BYTES, # Files: $OBJECTS, Last-Modified: ${LAST_FILE:5:19}\n"
        RETURN_CODE="2"
    fi
    PERF_DATA+="${CONTAINER}_size=$BYTES ${CONTAINER}_files=$OBJECTS "
done
echo "$OUTPUT | $PERF_DATA"
exit ${RETURN_CODE} 
